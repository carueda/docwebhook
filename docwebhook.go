package main

import (
	"fmt"
	"time"
	"net/http"
	"encoding/json"
	"io/ioutil"
	"os"

	"github.com/gorilla/mux"
	"github.com/codeskyblue/go-sh"
)

const version = "0.2.0"

type Repo struct {
	LocalRepo   string
	Destination string
}

type Config struct {
	Repos map[string]Repo
	Port string
}

var configFilename string = "./docwebhook.json"
var config Config

func main() {
	if len(os.Args) >= 2 {
		configFilename = os.Args[1]
	}
	fmt.Printf("== docwebhook %s starting ==\n", version)
	config, err := loadConfig()
	if err != nil {
		return
	}
	r := mux.NewRouter()
	r.HandleFunc("/", genDocHandler).
	Queries("repoName", "{.*}").
	Methods("POST")

	fmt.Printf("\ndocwebhook %s listening on %s\n", version, config.Port)
	http.ListenAndServe(":" + config.Port, r)
}

func genDocHandler(w http.ResponseWriter, r *http.Request) {
	repoName := r.URL.Query().Get("repoName")
	config, err := loadConfig()
	if err == nil {
		repo, ok := config.Repos[repoName]
		if ok {
			output, err := genDoc(repoName, repo)
			if len(err) == 0 {
				w.Write([]byte(output + "\n"))
			} else {
				w.Write([]byte(err + "\n"))
			}
		} else {
			w.Write([]byte(fmt.Sprintf("invalid repoName: %s\n", repoName)))
		}
	} else {
		w.Write([]byte(fmt.Sprintf("could not load config: %s\n", err)))
	}
}

func loadConfig() (config Config, err error) {
	fmt.Printf("reading %s ...\n", configFilename)
	file, err := ioutil.ReadFile(configFilename)
	if err == nil {
		err = json.Unmarshal(file, &config)
		if err == nil {
			fmt.Printf("configured repos:\n")
			for repoName, repo := range config.Repos {
				fmt.Printf("  '%s':\n", repoName)
				fmt.Printf("         localRepo   : '%s'\n", repo.LocalRepo)
				fmt.Printf("         destination : '%s'\n", repo.Destination)
			}
		}
	} else {
		fmt.Printf("docwebhook: could not load %s: %s\n", configFilename, err)
	}
	return config, err
}

func genDoc(repoName string, repo Repo) (output string, err string) {
	fmt.Printf("%s: docwebhook %s processing repo: %s\n", time.Now(), version, repo)

	shell := sh.NewSession()
	shell.SetDir(repo.LocalRepo)

	_, err = exec(shell, "git", "pull", "origin", "master")
	if len(err) > 0 { return }

	_, err = exec(shell, "mkdocs", "build", "--clean")
	if len(err) > 0 { return }

	_, err = exec(shell, "cp", "-r", "site/.", repo.Destination)
	if len(err) > 0 { return }

	output = fmt.Sprintf(`
    Documentation generated.
      localRepo:   %s
      destination: %s`,
		repo.LocalRepo, repo.Destination)

	return output, err
}

func exec(shell *sh.Session, cmd string, cmdArgs ...interface{}) (output string, err string) {
	fmt.Println(cmd, cmdArgs)
	o, e := shell.Command(cmd, cmdArgs...).Output()
	if e == nil {
		output = string(o)
		fmt.Println(string(output))
	} else {
		err = fmt.Sprintf("'%s %s' error: %s", cmd, cmdArgs, e)
		fmt.Println(err)
	}
	return output, err
}
