## docwebhook

Little http service, implemented in [Go](https://golang.org/), 
intended to be used as a "webhook" to generate documentation for
a given [MkDocs](http://www.mkdocs.org/) based repository.

Basically, for a requested repository, the service runs the following under the associated local clone:

    $ cd /path/to/clone
    $ git pull origin master
    $ mkdocs build --clean
    $ cp -r site/. /path/to/destination

Configuration is given as a parameter pointing to a JSON file with the following structure:

    ```json
    {
      "repos" : {
        "foo": {
          "localRepo":   "/path/to/foo-repo",
          "destination": "/var/www/html/doc/foo/"
        },
        "baz": {
          "localRepo":   "/path/to/baz-repo",
          "destination": "/var/www/html/doc/baz/"
        },
      },
    
      "mkdocs": "/path/to/executable/mkdocs",
      "port": "8000"
    }
    ```


### Get it

    $ go get bitbucket.org/carueda/docwebhook
    $ go install bitbucket.org/carueda/docwebhook

### Run it

    $ $GOPATH/bin/docwebhook /path/to/docwebhook/config.json
    ...
    docwebhook 0.2.0 listening on 8000

The service only has a POST route expecting a `repoName` parameter whose value should correspond to
one of the keys under the `"repos"` entry in the given configuration.


Example of exercising the service (using [HTTPie](https://github.com/jkbrzt/httpie)):

    $ http post 'https://okeanids.mbari.org/docwebhook?repoName=foo'
    HTTP/1.1 200 OK
    ...

    Documentation generated.
      localRepo:   /path/to/foo-repo
      destination: /var/www/html/doc/foo/


### crontab

On tethysdash I've launched the service with output to `/home/carueda/docwebhook-output/_output`.

I also set up a crontab to launch it at boot time:

    PATH=/home/carueda/gowork/bin:...other paths...
    
    @reboot docwebhook >> /home/carueda/docwebhook-output/_output

where `/home/carueda/gowork` is the value of my `$GOPATH`.
